//
// Created by jacob on 17.12.19.
//

#ifndef GAZEBO_GPULASERCAMERASETTING_HH_
#define GAZEBO_GPULASERCAMERASETTING_HH_

namespace gazebo
{

namespace rendering
{
  class GpuLaserCameraSetting
  {
  public: double azimuthOffset;
  public: double elevationOffset;
  };
}

}

#endif //GAZEBO_GPULASERCAMERASETTING_HH_
